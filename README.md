# <h2 align="center">Meu Portfólio</h2>

[![Status do Repositório](https://img.shields.io/badge/Repositório%20-Maintained-dark%20green.svg)](https://github.com/alexandresantosal91/alexandresantosal91.github.io)
[![Status do Site](https://img.shields.io/badge/Website%20Status-Online-green)](http://alexandresantosal91.github.io)
[![Autor](https://img.shields.io/badge/Author-Alexandre%20Santos-blue.svg)](https://www.linkedin.com/in/alexandresantosal/)
[![macbook-m1](https://img.shields.io/badge/MacBook-Air_M1_2021%20-blue.svg)](https://www.apple.com/br/macbook-air-m1/)
[![Lançamento Mais Recente](https://img.shields.io/badge/Latest%20Release-29%20jul%20de%202023-yellow.svg)](https://github.com/alexandresantosal91/alexandresantosal91.github.io/commits/main)
[![NPM](https://img.shields.io/npm/l/react)](https://github.com/alexandresantosal91/alexandresantosal91.github.io/blob/main/LICENSE)

<p align="justify">Olá, meu nome é Alexandre Santos e atualmente estou cursando Técnico em Análise e Desenvolvimento de Sistemas. Este site foi criado como um portfólio online para exibir minhas habilidades e presença na web. Aqui você encontrará informações detalhadas sobre meu currículo, minha biografia e poderá conferir meus projetos em destaque.</p>

[![Meu site de portfólio alternativo](assets/img/preview1.png)](http://alexandresantosal91.github.io)

<p align="center"><a href="https://alexandresantosal91-github-io.vercel.app" target="_blank">Clique Aqui para acessar o projeto</a></p>

 <p align="justify">Entre em contato comigo pelo e-mail alexandresantos_al@hotmail.com se tiver algum comentário ou ideia para o site. Se gostou, deixe uma avaliação de estrelas!</p>

